#include <stddef.h>
#include <vector>
#include <array>
#include <type_traits>

using namespace std;

template<bool B, typename T>
using Enable_if = typename std::enable_if<B,T>::type;

template<typename T, typename U>
using Common_type = typename common_type<T,U>::type;

template<typename T>
using Value_type = typename T::value_type;

constexpr bool All() { return true; }
   template<typename... Args>
constexpr bool All(bool b, Args... args)
{
   return b && All(args...);
}

namespace Matrix_impl {
   template<typename T, size_t N>
      struct Matrix_init {
         using type = initializer_list<typename Matrix_init<T,N-1>::type>;
      };

   template<typename T>
      struct Matrix_init<T,0>; // undefined on purpose

   template<typename T>
      struct Matrix_init<T,1> {
	 using type = initializer_list<T>;
      };


   template<typename List>
      bool check_non_jagged(const List& list)
      {
	 auto i = list.begin();
	 for (auto j = i+1; j!=list.end(); ++j)
            if(i->size()!=j->size())
	       return false;
	 return true;
      }

   template<typename T, typename Vec>
      void add_list(const T* first, const T* last, Vec& vec)
      {
	 vec.insert(vec.end(),first,last);
      }
   template<typename T, typename Vec> // nested initializer_lists
      void add_list(const initializer_list<T>* first, const initializer_list<T>* last, Vec& vec)
      {
         for (;first!=last;++first)
            add_list(first->begin(),first->end(),vec);
      }
   template<typename T, typename Vec>
      void insert_flat(initializer_list<T> list, Vec& vec)
      {
	 add_list(list.begin(),list.end(),vec);
      }

   template<size_t N, typename I, typename List>
      Enable_if<(N>1),void> add_extents(I& first, const List& list)
      {
	 assert(check_non_jagged(list));
	 *first = list.size();
	 add_extents<N-1>(++first,*list.begin());
      }
   template<size_t N, typename I, typename List>
      Enable_if<(N==1),void> add_extents(I& first, const List& list)
      {
	 *first++ = list.size();
	 // we reached the deepest nesting
      }

   template<size_t N, typename List>
      array<size_t, N> derive_extents(const List& list)
      {
	 array<size_t,N> a;
	 auto f = a.begin();
	 add_extents<N>(f,list); // put extents from list into f[]
	 return a;
      }
}

template<typename T, size_t N>
using Matrix_initializer = typename Matrix_impl::Matrix_init<T, N>::type;

template<size_t N>
struct Matrix_slice {
   // N extents
   template<typename... Dims>
      size_t operator()(Dims... dims) const;
   array<size_t,N> extents;
   array<size_t,N> strides;

#if 0
   Matrix_slice() = default;
   // an empty matrix: no elements
   Matrix_slice(size_t s, initializer_list<size_t> exts); // extents
   Matrix_slice(size_t s, initializer_list<size_t> exts, initializer_list<size_t> strs);// extents and strides
   template<typename... Dims>
      Matrix_slice(Dims... dims);
      size_t siz e;
   size_t star t;
   // total number of elements
   // star ting offset
   // number of elements in each dimension
   // offsets between elements in each dimension
#endif
};

template<size_t N>
template<typename... Dims>
size_t Matrix_slice<N>::operator()(Dims... dims) const
{
   static_assert(sizeof...(Dims) == N, "");
   size_t args[N] { size_t(dims)... };
   // Copy arguments into an array
   return inner_product(args,args+N,strides.begin(),size_t(0));
}

template<typename T, size_t N>
class Matrix_ref {
#if 0
   public:
      Matrix_ref(const Matrix_slice<N>& s, T∗ p) :desc{s}, ptr{p} {}
      // ... mostly like Matr ix ...

   private:
      Matrix_slice<N> desc;
      T* ptr;
#endif
};


template<typename T, size_t N>
class Matrix {
   public:
      using value_type = T;

      template<typename... Exts>
	 explicit Matrix(Exts... exts); // specify the extents
      Matrix(Matrix_initializer<T,N>);
      template<typename U>
	 Matrix(const Matrix_ref<U,N>&);
      template<typename U>
	 Matrix& operator=(const Matrix_ref<U,N>&);

      Matrix_ref<T,N-1> operator[](size_t i) { /*return row(i);*/}
      Matrix& operator+=(const T& value);

      template<typename F>
	 Matrix& apply(F f); // f(x) for every element x

      template<typename M, typename F>
	 Matrix& apply(const M& m, F f); // f(x,mx) for corresponding elements

      template<typename M>
	 Matrix& operator+=(const M& x);

      Matrix_ref<T,N-1> row(size_t n);
#if 0
      static constexpr size_t order = N;
      using iterator = typename std::vector<T>::iterator;
      using const_iterator = typename std::vector<T>::const_iterator;
      Matrix() = default;
      Matrix(Matrix&&) = default;
      Matrix& operator=(Matrix&&) = default;
      Matrix(Matrix const&) = default;
      Matrix& operator=(Matrix const&) = default;
      Matrix() = default;
      // move
      // copy
      // construct from Matrix_ref
      // assign from Matrix_ref
      Matrix& operator=(Matrix_initializer<T,N>); // initialize from list
      // assign from list
      template<typename U>
	 Matrix(initializer_list<U>) = delete;
      template<typename U>
	 Matrix& operator=(initializer_list<U>) = delete;
      // don’t use {} except for elements
      static constexpr size_t order() { return N; }
      size_t extent(size_t n) const { return desc.extents[n]; }
      size_t size() const { return elems.size(); }
      const Matrix_slice<N>& descriptor() const { return desc; } // number of dimensions
      // #elements in the nth dimension
      // total number of elements
      // the slice defining subscripting
      T* data() { return elems.data(); }
      const T* data() const { return elems.data(); } // ‘‘flat’’ element access

      template<typename... Args>
	 // m(i,j,k) subscripting with integers
	 Enable_if<Matrix_impl::Requesting_element<Args...>(), T&>
	 operator()(Args... args);
      template<typename... Args>
	 Enable_if<Matrix_impl::Requesting_element<Args...>(), const T&>
	 operator()(Args... args) const;
      template<typename... Args>
	 // m(s1,s2,s3) subscripting with slices
	 Enable_if<Matrix_impl::Requesting_slice<Args...>(), Matrix_ref<T, N>>
	 operator()(const Args&... args);
      template<typename... Args>
	 Enable_if<Matrix_impl::Requesting_slice<Args...>(), Matrix_ref<const T,N>>
	 operator()(const Args&... args) const;
      // m[i] row access
      Matrix_ref<const T,N−1> operator[](size_t i) const { return row(i); }
      Matrix_ref<const T,N−1> row(siz e_t n) const; // row access
      Matrix_ref<T,N−1> col(size_t n);
      Matrix_ref<const T,N−1> col(size_t n) const;
      // ...

      Matrix& operator=(const T& value); // assignment with scalar
      Matrix& operator−=(const T& value);
      Matrix& operator∗=(const T& value);
      Matrix& operator/=(const T& value);
      Matrix& operator%=(const T& value); // scalar addition
      // scalar subtraction
      // scalar multiplication
      // scalar division
      // scalar modulo
      template<typename M>
	 Matrix& operator−=(const M& x);

#endif
   private:
      Matrix_slice<N> desc;
      std::vector<T> elems;
};
   template<typename T, size_t N>
Matrix_ref<T,N-1> Matrix<T,N>::row(size_t n)
{
   //assert(n<rows());
   Matrix_slice<N-1> row;
   Matrix_impl::slice_dim<0>(n,desc,row);
   return {row,data()};
}

template<typename T, size_t N>
template<typename... Exts>
Matrix<T,N>::Matrix(Exts... exts)
   :desc{exts...},
   // copy extents
   elems(desc.size) // allocate desc.size elements and default initialize them
{}

template<typename T, size_t N>
Matrix<T, N>::Matrix(Matrix_initializer<T,N> init)
{
   Matrix_impl::derive_extents(init,desc.extents);
   elems.reserve(desc.size);
   Matrix_impl::insert_flat(init,elems);
   assert(elems.size() == desc.size);
}

template<typename T, size_t N>
   template<typename U>
Matrix<T,N>::Matrix(const Matrix_ref<U,N>& x)
   :desc{x.desc}, elems{x.begin(),x.end()} // copy desc and elements
{
   //static_assert(Convertible<U,T>(),"Matrix constructor: incompatible element types");
   static_assert(is_convertible<U,T>::value,"Matrix constructor: incompatible element types");
}

template<typename T, size_t N>
   template<typename U>
Matrix<T,N>& Matrix<T,N>::operator=(const Matrix_ref<U,N>& x)
{
   //static_assert(Convertible<U,T>(),"Matrix =: incompatible element types");
   static_assert(is_convertible<U,T>::value,"Matrix constructor: incompatible element types");
   desc = x.desc;
   elems.assign(x.begin(),x.end());
   return *this;
}

   template<typename T, size_t N>
Matrix<T,N>& Matrix<T,N>::operator+=(const T& val)
{
   return apply([&](T& a) { a+=val; } ); // using a lambda (§11.4)
}

template<typename T, size_t N>
   template<typename F>
Matrix<T,N>& Matrix<T,N>::apply(F f)
{
   for (auto& x : elems) f(x);
   // this loop uses stride iterators
   return *this;
}

#if 0
template<typename T, size_t N>
   template<typename M, typename F>
//Enable_if<Matrix_type<M>(),Matrix<T,N>&> Matrix<T,N>::apply(M& m, F f)
Matrix<T,N>& Matrix<T,N>::apply(M& m, F f)
{
   assert(same_extents(desc,m.descriptor()));
   // make sure sizes match
   for (auto i = begin(), j = m.begin(); i!=end(); ++i, ++j)
      f(*i,*j);
   return *this;
}
#endif

   template<typename T, size_t N>
Matrix<T,N> operator+(const Matrix<T,N>& m, const T& val)
{
   Matrix<T,N> res = m;
   res+=val;
   return res;
}

template<typename T, size_t N>
   template<typename M>
//Enable_if<Matrix_type<M>(),Matrix<T,N>&> Matrix<T,N>::operator+=(const M& m)
Matrix<T,N>& Matrix<T,N>::operator+=(const M& m)
{
   static_assert(m.order()==N,"+=: mismatched Matrix dimensions");
   assert(same_extents(desc,m.descriptor()));
   // make sure sizes match
   return apply(m, [](T& a,Value_type<M>&b) { a+=b; });
}
#if 0
   template<typename T, size_t N>
Matrix<T,N> operator+(const Matrix<T,N>& a, const Matrix<T,N>& b)
{
   Matrix<T,N> res = a;
   res+=b;
   return res;
}
#endif

template<typename T, typename T2, size_t N,
   typename RT = Matrix<Common_type<Value_type<T>,Value_type<T2>>,N>>
Matrix<RT,N> operator+(const Matrix<T,N>& a, const Matrix<T2,N>& b)
{
   Matrix<RT,N> res = a;
   res+=b;
   return res;
}

   template<typename T, size_t N>
Matrix<T,N> operator+(const Matrix_ref<T,N>& x, const T& n)
{
   Matrix<T,N> res = x;
   res+=n;
   return res;
}

   template<typename T>
Matrix<T,2> operator*(const Matrix<T,1>& u, const Matrix<T,1>& v)
{
   const size_t n = u.extent(0);
   const size_t m = v.extent(0);
   Matrix<T,2> res(n,m);
   // an n-by-m matrix
   for (size_t i = 0; i!=n; ++i)
      for (size_t j = 0; j!=m; ++j)
	 res(i,j) = u[i]*v[j];
   return res;
}

   template<typename T>
Matrix<T,1> operator*(const Matrix<T,2>& m, const Matrix<T,1>& v)
{
   assert(m.extent(1)==v.extent(0));
   const size_t n = m.extent(0);
   Matrix<T,1> res(n);
   for (size_t i = 0; i!=n; ++i)
      for (size_t j = 0; j!=n; ++j)
	 res(i) += m(i,j)*v(j);
   return res;
}

   template<typename T>
Matrix<T,2> operator*(const Matrix<T,2>& m1, const Matrix<T,2>& m2)
{
   const size_t n = m1.extent(0);
   const size_t m = m1.extent(1);
   assert(m==m2.extent(0));
   // columns must match rows
   const size_t p = m2.extent(1);
   Matrix<T,2> res(n,p);
   for (size_t i = 0; i!=n; ++i)
      for (size_t j = 0; j!=m; ++j)
	 for (size_t k = 0; k!=p; ++k)
	    res(i,j) = m1(i,k)*m2(k,j);
   return res;
}


#if 0
enum class Piece { none, cross, naught };
Matrix<Piece,2> board1 {
   {Piece::none, Piece::none , Piece::none},
      {Piece::none, Piece::none , Piece::none},
      {Piece::none, Piece::none , Piece::cross}
};
Matrix<Piece,2> board2(3,3); // OK
Matrix<Piece,2> board3 {3,3}; // error : constr uctor from initializer_list<int> deleted
#endif

int main()
{

}

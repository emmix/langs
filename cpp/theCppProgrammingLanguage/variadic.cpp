
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <list>


template<typename T>
T adder(T v) {
  return v;
}

template<typename T, typename... Args>
T adder(T first, Args... args) {
  std::cout << __PRETTY_FUNCTION__ << "\n";
  return first + adder(args...);
}

long sum = adder(1, 2, 3, 8, 7);

//std::string s1 = "x", s2 = "aa", s3 = "bb", s4 = "yy";
//std::string ssum = adder(s1, s2, s3, s4);

template <template <typename, typename...> class ContainerType,
          typename ValueType, typename... Args>
void print_container(const ContainerType<ValueType, Args...>& c) {
  for (const auto& v : c) {
    std::cout << v << ' ';
  }
  std::cout << '\n';
}
// Implement << for pairs: this is needed to print out mappings where range
// iteration goes over (key, value) pairs.
template <typename T, typename U>
std::ostream& operator<<(std::ostream& out, const std::pair<T, U>& p) {
  out << "[" << p.first << ", " << p.second << "]";
  return out;
}


int main()
{
   std::vector<double> vd{3.14, 8.1, 3.2, 1.0};
   print_container(vd);

   std::list<int> li{1, 2, 3, 5};
   print_container(li);

   std::map<std::string, int> msi{{"foo", 42}, {"bar", 81}, {"bazzo", 4}};
   print_container(msi);



}

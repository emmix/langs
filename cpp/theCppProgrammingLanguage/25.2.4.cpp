
#include <vector>

using namespace std;

struct Record {

};

struct Entry {

};

template<typename T, template<typename> class C>
class Xrefd {
   C<T> mems;
   C<T*> refs;
   // ...
};

template<typename T>
using My_vec = vector<T>;

Xrefd<Entry,My_vec> x1;
// use default allocator
// store cross references for Entrys in a vector
template<typename T>
class My_container {
   // ...
};
Xrefd<Record,My_container> x2;
// store cross references for Records in a My_container

template<typename C, typename C2>
class Xrefd2 {
   C mems;
   C2 refs;
   // ...
};
Xrefd2<vector<Entry>, set<Entry*> > x;

int main()
{

}

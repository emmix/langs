
template<>
struct Tuple { Tuple() {} };

template<typename T1>
struct Tuple : Tuple<> {
   T1 x;
   // 0-tuple
   // 1-tuple
   using Base = Tuple<>;
   Base* base() { return static_cast<Base*>(this); }
   const Base* base() const { return static_cast<const Base*>(this); }
   Tuple(const T1& t1) :Base{}, x{t1} { }
};

template<typename T1, typename T2>
struct Tuple : Tuple<T2> {
   T1 x;
   // 2-tuple, lay out: T2 before T1
   using Base = Tuple<T2>;
   Base* base() { return static_cast<Base*>(this); }
   const Base* base() const { return static_cast<const Base*>(this); }
   Tuple(const T1& t1, const T2& t2) :Base{t2}, x{t1} { }
};

template<typename T1, typename T2, typename T3>
struct Tuple<T1, T2, T3> : Tuple<T2, T3> {
   // 3-tuple, lay out: {T2,T3} before T1
   T1 x;
   using Base = Tuple<T2, T3>;
   Base* base() { return static_cast<Base*>(this); }
   const Base* base() const { return static_cast<const Base*>(this); }
   Tuple(const T1& t1, const T2& t2, const T3& t3) :Base{t2, t3}, x{t1} { }
};

int main()
{

}

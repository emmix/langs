


template<typename Iter, typename Val>
Val sum(Iter first, Iter last)
{
   Val s=0;
   while(first!=last) {
      s = s + *first;
      ++first;
   }

   return s;
}

struct Node {Node* next; int data;};

Node* operator++(Node* p) { return p->next;}
int operator*(Node* p) { return p->data;}
Node* end(Node* lst) { return nullptr;}

void test(Node* lst) {
   int s = sum<int*>(lst, end(lst));
}

int main()
{

   return 0;
}

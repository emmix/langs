#include <initializer_list>
#include <iostream>


  template <typename T, std::size_t N>
    struct matrix_init
    {
      using type = std::initializer_list<typename matrix_init<T, N - 1>::type>;
    };

  template <typename T>
    struct matrix_init<T, 1>
    {
      using type = std::initializer_list<T>;
    };

  // This is not defined on purpose!
  template <typename T>
    struct matrix_init<T, 0>;

  template <typename T, std::size_t N>
      using matrix_initializer = typename matrix_init<T, N>::type;

int main()
{
   matrix_init<int, 1>::type a = {1, 2};
   matrix_initializer<int, 1> b = {1, 2};
   std::initializer_list<int> c = {1, 2};
   std::initializer_list<std::initializer_list<int>> d = {{1,2},{1,2}, {1,2}};
   std::cout<<d.size()<<std::endl;
   std::cout<<(*d.begin()).size()<<std::endl;


}




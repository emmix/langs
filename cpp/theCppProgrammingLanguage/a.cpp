
class Base {
   protected:
      int foo;
   public:
      int method(int p) {
	 return foo + p;
      }
};

#if 0
struct Point {
     double cx, cy;
};

class Derived : public Base {
   public:
        int method(int p) {
	       return foo + bar + p;
	         }
   protected:
	  int bar, baz;
	    Point a_point;
	      char c;
};

#endif

int main(int argc, char** argv) {
     //return sizeof(Derived);
     return sizeof(Base);
}

/*
 * =====================================================================================
 *
 *       Filename:  a.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/09/2016 02:02:57 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>


struct A {
   int a = 1;
};

struct B: A{

   double a = 2.0;
};


int main()
{
   B b;
   std::cout<<b.a<<std::endl;
   A& a = b;
   std::cout<<a.a<<std::endl;

}

#include <memory>

using namespace std;
namespace bjarne{

template<class T, class A=std::allocator<T> >
struct vector_base {
   T* elem;
   T* space;
   T* last;
   A alloc;

   vector_base(const A& a, typename A::size_type n)
      :alloc(a), elem{alloc.allocate(n)}, space{elem+n}, last{elem+n}{}
   ~vector_base() {alloc.deallocate(elem, last-elem);}

   vector_base(const vector_base&) = delete;
   vector_base& operator=(const vector_base&) = delete;

   vector_base(vector_base&&);
   vector_base& operator=(vector_base&&);
};

template<class T, class A>
vector_base<T, A>::vector_base(vector_base&& a)
   :alloc{a.alloc},
   elem{a.alem},
   space{a.space},
   last{a.space}
{
   a.elem = a.space = a.last = nullptr;
}

template<class T, class A>
vector_base<T, A>& vector_base<T,A>::operator=(vector_base&& a)
{
   swap(*this, a);
   return *this;
}


template<class T, class A=std::allocator<T> >
class vector {
   vector_base<T, A> vb;
   void destroy_eleents();
public:
   using size_type = unsigned int;
   explicit vector(size_type n, const T& val = T(), const A& = A());
   vector(const vector& a);

   vector& operator=(vector&& a);
   vector(vector&& a);

   ~vector() {destroy_eleents();}

};

template<class T, class A>
void vector<T, A>::destroy_eleents()
{
   for (T* p = vb.elem; p!=vb.space; ++p)
      p->~T();
   // destroy element (§17.2.4)
   vb.space=vb.elem;
}

template<class T, class A>
vector<T, A>::vector(size_type n, const T& val, const A& a)
   :vb{a,n}
{
   uninitialized_fill(vb.elem,vb.elem+n,val); // make n copies of val
}

template<class T, class A>
vector<T,A>::vector(const vector<T,A>& a)
   :vb{a.alloc,a.size()}
{
   uninitialized_copy(a.begin(),a.end(),vb.elem);
}

template<class T, class A>
vector<T,A>::vector(vector&& a)
:vb{move(a.vb)}
// transfer ownership
{
}

template<class T, class A>
vector<T,A>& vector<T,A>::operator=(vector&& a)
{
   //clear();
   // destroy elements
   swap(*this,a);
   // transfer ownership
}






}

int main()
{

   //bjarne::vector<int> v(10);
   return 0;
}

   

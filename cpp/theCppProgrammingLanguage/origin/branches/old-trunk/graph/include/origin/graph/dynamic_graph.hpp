// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_GRAPH_DYNAMIC_GRAPH_HPP
#define ORIGIN_GRAPH_DYNAMIC_GRAPH_HPP

#error Not implemented

// The dynamic graph library is an adjacency list data structure that is
// fully dynamic in that it supports the unrestricted addition and deletion
// of vertices and edges.

#endif

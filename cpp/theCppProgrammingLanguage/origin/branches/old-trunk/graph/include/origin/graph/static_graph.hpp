// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_GRAPH_STATIC_GRAPH_HPP
#define ORIGIN_GRAPH_STATIC_GRAPH_HPP

#error Not implemented

// The static graph library is an adjacency list implementation for static
// (i.e., generally immutable) graphs based on the paper, "Design and 
// Implementation of Efficient Data Types for Static Graphs".

#endif

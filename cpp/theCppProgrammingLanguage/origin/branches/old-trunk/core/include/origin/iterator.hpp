// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_ITERATOR_HPP
#define ORIGIN_ITERATOR_HPP

#include <cassert>

// Core abstractions
#include <origin/iterator/core.hpp>

// Iterator adaptors
#include <origin/iterator/facade.hpp>
#include <origin/iterator/counter.hpp>
#include <origin/iterator/stride.hpp>
#include <origin/iterator/filter.hpp>
#include <origin/iterator/transform.hpp>

#endif

// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011-2012 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_RANGE_HPP
#define ORIGIN_RANGE_HPP

#include <iostream>

// Core abstractions
#include <origin/range/core.hpp>

// Range types and adaptors
#include <origin/range/bounded.hpp>
#include <origin/range/filter.hpp>

#endif

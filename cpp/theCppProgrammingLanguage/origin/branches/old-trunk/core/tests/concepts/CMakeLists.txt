# Copyright (c) 2008-2010 Kent State University
# Copyright (c) 2011 Texas A&M University
#
# This file is distributed under the MIT License. See the accompanying file
# LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
# and conditions.

origin_run_test(concepts_copyable copyable.cpp)
origin_run_test(concepts_equality_comparable equality_comparable.cpp)
origin_run_test(concepts_totally_ordered totally_ordered.cpp)

origin_run_test(concepts_value_type value_type.cpp)
origin_run_test(concepts_difference_type difference_type.cpp)

origin_run_test(concepts_streamable streamable.cpp)

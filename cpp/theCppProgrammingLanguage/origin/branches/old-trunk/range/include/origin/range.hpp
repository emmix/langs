// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_RANGE_HPP
#define ORIGIN_RANGE_HPP

#include <origin/range/iterator_range.hpp>
#include <origin/range/container_range.hpp>
#include <origin/range/filter_range.hpp>
#include <origin/range/enumerate_range.hpp>
#include <origin/range/zip_range.hpp>

/**
 * @defgroup Range
 * The Range module contains range adaptors and other range-based support
 * abstractions.
 *
 * @note The Range library is a likely candidate for migration into core.
 */

#endif

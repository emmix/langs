// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#include <cassert>
#include <iostream>
#include <algorithm>

#include <origin/real.hpp>

using namespace std;
using namespace origin;

int main()
{
	real i = 10.23234598343948523895235890345423543423453;
	cout << i << '\n';
}

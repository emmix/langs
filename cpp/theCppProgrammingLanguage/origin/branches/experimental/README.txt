
The experimental version of the Origin trunk is *not* compatible with the
C++ standard library. It is purely experimental and intended to evaluate new
concept and library ideas using an experimental (i.e., research) compiler.

In particular this project focuses the development and application of 
template constraints.



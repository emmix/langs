// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011-2012 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_TYPE_HPP
#define ORIGIN_TYPE_HPP

#include <origin/type/traits.hpp>
#include <origin/type/typestr.hpp>

#endif

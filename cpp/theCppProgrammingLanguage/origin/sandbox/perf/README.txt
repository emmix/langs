
The Origin.Perf library provides tools for performance testing and profiling
C++0x projects.

There are currently two sets of tools used to measure performance:

- Timers
- Cache introspection (using PAPI)

== TODO ==
This library is slated to move into core and the configuration modules made
global.

// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011-2012 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#ifndef ORIGIN_GRAPH_ADJACECNY_VECTOR_DIRECTED_HPP
#define ORIGIN_GRAPH_ADJACECNY_VECTOR_DIRECTED_HPP

#include <origin/graph/adjacency_vector/impl.hpp>

namespace origin {

} // namespace origin

#endif // ORIGIN_GRAPH_ADJACECNY_VECTOR_DIRECTED_HPP
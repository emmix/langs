// Copyright (c) 2008-2010 Kent State University
// Copyright (c) 2011 Texas A&M University
//
// This file is distributed under the MIT License. See the accompanying file
// LICENSE.txt or http://www.opensource.org/licenses/mit-license.php for terms
// and conditions.

#include <iostream>
#include <iterator>

#include <origin/heaps/binomial_heap.hpp>

#include "check_heap.hpp"

using namespace std;
using namespace origin;


int main()
{
  default_random_engine eng;
  
  binomial_tree<int> t0{0};
  binomial_tree<int> t1{1};
  assert(( t0.order() == 0 ));
  t0.link(t1);
  assert(( t0.order() == 1 ));
  assert(( t1. empty() ));
  
  check_heap<binomial_heap>(eng);

//   for(int i = 0; i < N; ++i) {
    // check_heap<mutable_binomial_heap>(eng);
    // check_mutable_heap<mutable_binomial_heap>(eng);
//   }
}

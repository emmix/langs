/*
 * =====================================================================================
 *
 *       Filename:  a.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年08月31日 18时03分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>

using namespace std;

class A {
   public:
   A(){cout<<__func__<<endl;}
   ~A(){cout<<__func__<<endl;}
};

double compute(double x) noexcept
//double compute(double x) 
{
   string s = "Courtney and Anya";
   A a;
   vector<double> tmp(1000000000000);
   // ...
}

int main()
{

   try{
   compute(10.0f);
   }catch(std::exception& e){
      cout<<e.what()<<endl;
   }
   while(1){
      sleep(2);
      cout<<"fff"<<endl;
   }

}

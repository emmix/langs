

#include <locale>
#include <iostream>

using namespace std;
int main()
{
   locale loc("");
   cout<<loc.name()<<endl;
   cout<<endl;
   cout<<locale().name()<<endl;
   cout<<endl;
   cout<<locale::classic().name()<<endl;
   cout<<endl;
   cout<<(locale::global(loc)).name()<<endl;
   cout<<endl;
   cout<<(locale::global(loc)).name()<<endl;
   cout<<endl;
}

#include <memory>

using namespace std;
namespace bjarne{

template<class T, class A=std::allocator<T> >

class vector {
private:
   T* elem;
   T* space;
   T* last;
   A alloc;

public:
   using size_type = unsigned int;
   explicit vector(size_type n, const T& val = T(), const A& = A());
};

#if 0
template<class T, class A>
vector<T, A>::vector(size_type n, const T& val, const A& a)
   :alloc(a)
{
  elem = alloc.allocate(n);

  T* p;

  try {
     T* end = elem + n;
     for (p=elem; p!=end; ++p) {
	alloc.construct(p, val);
     }
     last = space = p;
  }
  catch(...) {
     for (T* q=elem; q!=p; ++q) {
	alloc.destroy(q);
     }
     alloc.deallocate(elem, n);
     throw;
  }

}
#endif

template<class T, class A>
vector<T, A>::vector(size_type n, const T& val, const A& a)
   :alloc(a)
{
   elem = alloc.allocate(n);
   try {
      uninitialized_fill(elem, elem+n, val);
      space = last = elem + n;
   }
   catch(...) {
     alloc.deallocate(elem, n);
     throw;
   }
}


}


int main()
{

   bjarne::vector<int> v(10);
   return 0;
}

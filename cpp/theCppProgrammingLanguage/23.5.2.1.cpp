#include <iostream>
#include <iostream>

using namespace std;

template<typename T>
class Xref {
   public:
      Xref(int i, T* p)
	 :index{i}, elem{p}, owner{true}
      {std::cout<<*elem<<": "<<"Xref(int i, T* p)"<<std::endl;}
      Xref(int i, T& r)
	 :index{i}, elem{&r}, owner{false}
      {std::cout<<*elem<<": "<<"Xref(int i, T& r)"<<std::endl;}
      Xref(int i, T&& r)
	 :index{i}, elem{new T{move(r)}}, owner{true}
      {std::cout<<*elem<<": "<<"Xref(int i, T&& r)"<<std::endl;}
     ~Xref()
      {
	 if(owner) delete elem;
      }
      // ...
   private:
      int index;
      T* elem;
      bool owner;
};

int main()
{
   string x {"There and back again"};
   Xref<string> r1 {7,"Here"};
   Xref<string> r2 {9,x};
   Xref<string> r3 {3,new string{"There"}};
}



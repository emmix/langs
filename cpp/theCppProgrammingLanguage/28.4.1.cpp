

#include <iostream>
#include <type_traits>

template<typename T>
class vector {
   public:
      vector(size_t n, const T& val){std::cout<<"1"<<std::endl;}
      template < class Iter,
           class = typename std::enable_if<!std::is_integral<Iter>::value, Iter>::type>
      vector(Iter b, Iter e)
      {std::cout<<"2"<<std::endl;}
};

// enable_if example: two ways of using enable_if
#include <iostream>
#include <type_traits>

// 1. the return type (bool) is only valid if T is an integral type:
template <class T>
typename std::enable_if<std::is_integral<T>::value,bool>::type
  is_odd (T i) {return bool(i%2);}

// 2. the second template argument is only valid if T is an integral type:
template < class T,
           class = typename std::enable_if<std::is_integral<T>::value>::type>
bool is_even (T i) {return !bool(i%2);}

void foo(...)
{
   std::cout<<"foo"<<std::endl;
}

int main() {

  short int i = 1;    // code does not compile if type of i is not integral

  std::cout << std::boolalpha;
  std::cout << "i is odd: " << is_odd(i) << std::endl;
  std::cout << "i is even: " << is_even(i) << std::endl;

  vector<int>(1,2);
  foo(1);
  foo("he");
}












#include <iostream>

template<typename T>
void f(T& param)
{
	std::cout<<typeid(T).name()<<std::endl;
	std::cout<<typeid(T&).name()<<std::endl;
	std::cout<<param<<std::endl;
}	

template<typename T>
void fpointer(T* param)
{
	std::cout<<typeid(T).name()<<std::endl;
	std::cout<<typeid(T*).name()<<std::endl;
	std::cout<<param<<std::endl;
}
int main()
{
	int x=1;
	//f(27);
	f(x);
	int *p;
	f(p);

	fpointer(&x);
	fpointer(0x00);

}

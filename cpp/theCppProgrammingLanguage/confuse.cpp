

#include <iostream>
#include <vector>
using namespace std;


class myexception: public exception
{
  virtual const char* what() const throw()
  {
    return "My exception happened";
  }
} myex;

class BadClass {
   public:
      BadClass() {
	 throw myex;
      }
};


int main()
{
   //
   const int& a = int();
   cout<<a<<endl;

   //
   int j = 8;
   int *pointer = &j;
   cout<<"&pointer:"<<&pointer<<endl;
   cout<<"pointer:"<<pointer<<endl;
   cout<<"&j:"<<&j<<endl;
   cout<<"*pointer:"<<*pointer<<endl;
   cout<<"&*pointer:"<<&*pointer<<endl;

   // throw exception in vector construct
   try{
      vector<BadClass> vi(10);
   }
   catch(exception& e){
      cout<<e.what()<<endl;
   }
   try{
      vector<int> vi(10000000000);
   }
   catch(exception& e){
      cout<<e.what()<<endl;
   }
}

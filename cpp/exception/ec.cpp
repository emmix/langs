
// http://stackoverflow.com/questions/9760851/stderror-code-my-errorcheck-block-my-errorvalidate-my-erroraccept
//
#include <iostream>
#include <system_error>

namespace libbitcoin {

namespace error
{
    // Specific errors
    enum error_code_t
    {
        // storage errors
        missing_object = 1,
        object_already_exists,
        unspent_output,
        // transaction_pool errors
        bad_transaction,
        // network errors
        resolve_failed,
        network_unreachable,
        address_in_use,
        listen_failed,
        accept_failed,
        bad_stream,
        channel_stopped,
        channel_timeout,
        // validate
        check_block,
        accept_block,
        connect_block
    };

    // error_condition
    enum error_condition_t
    {
        // validate
        validate_failed = 1999
    };

    std::error_code make_error_code(error_code_t e);
    //std::error_condition make_error_condition(error_condition_t e);
    std::error_condition make_error_condition(error_code_t e);
}

class error_category_impl
  : public std::error_category
{
public:
    virtual const char* name() const noexcept;
    virtual std::string message(int ev) const;
    virtual std::error_condition default_error_condition(int ev) const noexcept;
};

const std::error_category& error_category();

} // libbitcoin

namespace std
{
    template <>
    struct is_error_code_enum<libbitcoin::error::error_code_t>
      : public true_type {};

#if 0
    template <>
    struct is_error_condition_enum<libbitcoin::error::error_condition_t>
      : public true_type {};
#endif
}

// -------------------------------------------------------------------

namespace libbitcoin {

namespace error {
std::error_code make_error_code(error_code_t e)
{
    return std::error_code(static_cast<int>(e), error_category());
}
std::error_condition make_error_condition(error_code_t e)
{
    return std::error_condition(static_cast<int>(e), error_category());
}
#if 0
std::error_condition make_error_condition(error_condition_t e)
{
    return std::error_condition(static_cast<int>(e), error_category());
}
#endif

}

const char* error_category_impl::name() const noexcept
{
    return "bitcoin";
}

std::string error_category_impl::message(int ev) const
{
    //error ec = static_cast<error>(ev);
    switch (ev)
    {
        case error::missing_object:
            return "Object does not exist";
        case error::object_already_exists:
            return "Matching previous object found";
        case error::unspent_output:
            return "Unspent output";
        case error::bad_transaction:
            return "Transaction failed to validate";
        case error::resolve_failed:
            return "Resolving hostname failed";
        case error::network_unreachable:
            return "Unable to reach remote network";
        case error::address_in_use:
            return "Address already in use";
        case error::listen_failed:
            return "Listen incoming connections failed";
        case error::accept_failed:
            return "Accept connection failed";
        case error::bad_stream:
            return "Bad stream";
        case error::channel_stopped:
            return "Channel stopped";
        case error::channel_timeout:
            return "Channel timed out";
        case error::check_block:
            return "Checkblk";
        default:
            return "Unknown error";
    }
}

std::error_condition
    error_category_impl::default_error_condition(int ev) const noexcept
{
    //error ec = static_cast<error>(ev);
    switch (ev)
    {
        case error::check_block:
        case error::accept_block:
        case error::connect_block:
            return std::error_condition(error::validate_failed, *this);
        default:
            return std::error_condition(ev, *this);
    }
}

const std::error_category& error_category()
{
    static error_category_impl instance;
    return instance;
}

} // libbitcoin

using namespace libbitcoin;

#include <assert.h>

int main()
{
    std::error_code ec = error::check_block;
    //assert(ec == error::validate_failed);
    assert(ec == error::check_block);
    std::cout << ec.message() << std::endl;
    //ec = error::missing_object;
    std::error_condition econd0 = error::make_error_condition(error::check_block);
    //std::error_condition econd1 = error::make_error_condition(error::validate_failed);
    std::error_condition econd2 = error_category().default_error_condition(error::validate_failed);
    std::cout << econd0.message()<<std::endl;
    //std::cout << econd1.message()<<std::endl;
    std::cout << econd2.message()<<std::endl;
    std::cout << econd2.value()<<std::endl;
    return 0;
}

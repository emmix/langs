/*
 * =====================================================================================
 *
 *       Filename:  a.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年04月06日 20时57分36秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>

class Test {

private:
   Test() {std::cout<<"construct"<<std::endl;}
   ~Test() {std::cout<<"destruct"<<std::endl;}

public:
   static Test& instance () {

      static Test test;
      return test;
   }
};

int main()
{
   std::cout<<"---------"<<std::endl;
   Test &ref = Test::instance();
   Test &ref2 = Test::instance();
   std::cout<<"---------"<<std::endl;
   return 0;
}

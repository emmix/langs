/*
 * =====================================================================================
 *
 *       Filename:  vector.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/06/2013 11:04:27 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <vector>
#include <string.h>
#include <stdint.h>
       #include <stdlib.h>

using namespace std;

static const int64_t COIN = 100000000;
static const int64_t CENT = 1000000;

inline int64_t atoi64(const char* psz)
{       
#ifdef _MSC_VER
	return _atoi64(psz);
#else
	return strtoll(psz, NULL, 10);
#endif  
}  

void Print (const vector<unsigned char>& v){
  //vector<int> v;
  for (int i=0; i<v.size();i++){
    cout << v[i] << endl;
  }
}
bool ParseMoney(const char* pszIn, int64_t& nRet)
{   
    string strWhole;
    int64_t nUnits = 0;
    const char* p = pszIn;
    while (isspace(*p))
        p++;
    for (; *p; p++)
    {   
        if (*p == '.')
        {
            p++;
            int64_t nMult = CENT*10;
            while (isdigit(*p) && (nMult > 0))
            {
                nUnits += nMult * (*p++ - '0');
                nMult /= 10;
            }
            break;
        }
        if (isspace(*p))
            break;
        if (!isdigit(*p))
            return false;
        strWhole.insert(strWhole.end(), *p);
    }
    for (; *p; p++)
        if (!isspace(*p))
            return false;
    if (strWhole.size() > 10) // guard against 63 bit overflow
        return false;
    if (nUnits < 0 || nUnits > COIN)
        return false;
    int64_t nWhole = atoi64(strWhole.c_str());
    int64_t nValue = nWhole*COIN + nUnits;

    nRet = nValue;
    return true;
}

main()
{
	const char* pszP2SH = "/P2SH/";
	//cout << std::vector<unsigned char>(pszP2SH, pszP2SH+strlen(pszP2SH));
	std::vector<unsigned char> v(pszP2SH, pszP2SH+strlen(pszP2SH));
	Print(v);

	int64_t n;
	ParseMoney("0.00000001", n);
	cout<<n<<endl;
}

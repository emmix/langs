/*
 * =====================================================================================
 *
 *       Filename:  a.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年08月04日 22时19分24秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>

class B {
   public:
      int b;
      virtual void f(){std::cout<<"b"<<std::endl;};
   //   virtual void f() = 0;
      void g(){};
      B();
};
class D:public B{
   public:
      int d;
      void f(){std::cout<<"d"<<std::endl;};
      D(){};
};

B::B()
{
   b++;
   f();
}

int main()
{
   //B bo;
   D o;

   return 0;
}


/*
 * =====================================================================================
 *
 *       Filename:  a.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年08月04日 22时45分14秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 *
 *
 */


#include <iostream>
using namespace std;
 
class A
{
private:
    int m_a;
public:
    A(int a): m_a(a) {show();}
    virtual ~A() {};
    virtual void show() const {cout << "show a: " << m_a << endl;}
    virtual void disp() const {cout << "disp a: " << m_a << endl;}
};
 
class B: public A
{
private:
    int m_b;
public:
    B(int a, int b): m_b(b), A(a) {}
    void show() const {A::show(); cout << "show b: " << m_b << endl;}
    void disp() const {A::disp(); cout << "disp b: " << m_b << endl;}
};
 
int main()
{
    A* pob3 = new B(100, 200);
    pob3->disp();
    delete pob3;
    return 0;
}




# IPC
https://www.ibm.com/developerworks/cn/aix/library/au-concurrent_boost/


http://theboostcpplibraries.com/
http://www.boost.org/doc/libs/


# online reference
http://www.boost.org/doc/
http://www.boost.org/doc/libs/1_41_0/doc/html/signals/tutorial.html

# thread
http://antonym.org/2009/05/threading-with-boost---part-i-creating-threads.html

# signal2
http://www.boost.org/doc/libs/1_55_0/doc/html/signals2.html
## boost::signals2::last_value
http://blog.csdn.net/pongba/article/details/1561006
http://www.boost.org/doc/libs/1_48_0/doc/html/boost/signals2/last_value.html

# boost::optional
http://www.boost.org/doc/libs/1_55_0/libs/optional/doc/html/index.html

# boost::algorithm::istarts_with

# boost:variant
http://www.boost.org/doc/libs/1_55_0/doc/html/variant.html



//#include <utility>
#include <vector>
#include <iostream>

template<class T, T... I>
struct integer_sequence
{
    typedef T value_type;

    static constexpr size_t size() noexcept;
};

template<size_t... I>
  using index_sequence = integer_sequence<size_t, I...>;

template<class T, T N>
  using make_integer_sequence = integer_sequence<T, 0, 1, ..., N-1>;
template<size_t N>
  using make_index_sequence = make_integer_sequence<size_t, N>;

template<class... T>
  using index_sequence_for = make_index_sequence<sizeof...(T)>;

   template<std::size_t... I>
std::vector<std::size_t> make_index_vector(index_sequence<I...>)
{
   return {I...};
}

int main()
{
   auto vec = make_index_vector(make_index_sequence<10>());
   for(auto i : vec) {
      std::cout << i << ' ';
   }
   std::cout << std::endl;
}



template< typename ...Tuples >
typelist_cat_t<
    typelist< as_typelist_t< Tuples >...>
>
tuple_cat(Tuples &&... tpls>;

template<template<typename...> class C, typename List>
struct typelist_apply;

template<template<typename...> class C, typename... List>
struct typelist_apply<C, typelist<List...>>
{
    using type = C<List...>;
};






#include <vector>
#include <iostream>
#include <typeinfo>

template <int... Is>
struct Seq { using type = Seq; };

// primary template
template <int I, class S>
struct PushBack;
// specialization for Seq<I0,I1,(...)>
#if 0
template <int I, int... Is>
struct PushBack<I, Seq<Is...>> {
  using type = Seq<Is..., I>;
};
#endif
template <int I, int... Is>
struct PushBack<I, Seq<Is...>> : Seq<Is..., I> {};

template <int N>
struct MakeSeq
{
     using type = typename PushBack<N-1, typename MakeSeq<N-2>::type>::type;
};

template <> struct MakeSeq<1> { using type = Seq<0>; };
template <> struct MakeSeq<0> { using type = Seq<>; };



template<int... I>
std::vector<int> make_index_vector(Seq<I...>)
{
   return {I...};
}

int main()
{
   auto vec = make_index_vector(PushBack<5, Seq<3,4>>::type());
   for(auto i : vec) {
      std::cout << i << ' ';
   }
   std::cout << std::endl;
}

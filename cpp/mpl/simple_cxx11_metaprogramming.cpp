
#include <tuple>
#include <typeinfo>
#include <iostream>

// mp_list
template<class... T> struct mp_list{};

// mp_rename
template<class A, template<class...> class B> struct mp_rename_impl;
template<template<class...> class A, class... T, template<class...> class B>
struct mp_rename_impl<A<T...>, B>
{
   using type = B<T...>;
};
template<class A, template<class...> class B>
using mp_rename = typename mp_rename_impl<A, B>::type;

// mp_length
template<class... T> using mp_length = std::integral_constant<std::size_t, sizeof...(T)>;

// mp_size
template<class L> struct mp_size_impl;
/*
template<class... T> struct mp_size_impl<mp_list<T...>>
{
   using type = std::integral_constant<std::size_t, sizeof...(T)>;
};
*/
template<template<class...> class L, class... T> struct mp_size_impl<L<T...>>
{
   //using type = std::integral_constant<std::size_t, sizeof...(T)>;
   using type = mp_length<T...>;
};
template<class L> using mp_size = typename mp_size_impl<L>::type;


int main()
{
   using list = mp_list<int, char>;
   mp_rename<list, std::tuple> t(1, 1);
   std::cout<<typeid(t).name()<<std::endl;
   std::cout<<typeid(list).name()<<std::endl;
   std::cout<<mp_size<list>::value<<std::endl;
}

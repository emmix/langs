#include <tuple>
#include <iostream>
#include <string>
#include <utility>

int func1(int arg1, char arg2, double arg3, const std::string& arg4)
{
  std::cout << "call func1(" << arg1 << ", " << arg2 << ", " << arg3 << ", " << arg4 << ")" << std::endl;
  return 0;
}

int func2(int arg1, int arg2)
{
  std::cout << "call func2(" << arg1 << ", " << arg2 << ")" << std::endl;
  return arg1 + arg2;
}

template<typename F, typename T, std::size_t... I>
auto apply_impl(F f, const T& t, std::index_sequence<I...>) -> decltype(f(std::get<I>(t)...))
{
  return f(std::get<I>(t)...);
}

template<typename F, typename T>
auto apply(F f, const T& t) -> decltype(apply_impl(f, t, std::make_index_sequence<std::tuple_size<T>::value>()))
{
  return apply_impl(f, t, std::make_index_sequence<std::tuple_size<T>::value>());
}

int main()
{
  using namespace std::literals::string_literals;
  auto tuple1 = std::make_tuple(1, 'A', 1.2, "破晓的博客"s);
  auto result1 = apply(func1, tuple1);
  std::cout << "result1 = " << result1 << std::endl;

  auto tuple2 = std::make_tuple(1, 2);
  auto result2 = apply(func2, tuple2);
  std::cout << "result2 = " << result2 << std::endl;
}
